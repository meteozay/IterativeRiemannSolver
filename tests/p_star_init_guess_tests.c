/*
 * p_star_init_guess_tests.c
 */


#include <cgreen/cgreen.h>
#include "./../src/p_star_init_guess.h"


#define RHO 1.23
#define VX 2.34
#define P 3.45


hydro_t ql, qr;


Describe(p_star_init_guess);
BeforeEach(p_star_init_guess) {};
AfterEach(p_star_init_guess) {};


Ensure(p_star_init_guess, guess_a_propper_value_for_p_star)
{
  double p_star = p_star_init_guess(
    &(hydro_t) {.rho = RHO, .u = VX, .p = P},
    &(hydro_t) {.rho = RHO, .u = VX, .p = P},
    vx_h
  );
  assert_that_double(p_star, is_equal_to_double(P));

  p_star = p_star_init_guess(
    &(hydro_t) {.rho = RHO, .u = VX, .p = P},
    &(hydro_t) {.rho = 5 * RHO, .u = VX, .p = P},
    vx_h
  );
  assert_that_double(p_star, is_equal_to_double(P));

  p_star = p_star_init_guess(
    &(hydro_t) {.rho = RHO, .u = VX, .p = P},
    &(hydro_t) {.rho = RHO, .u = VX, .p = 2 * P},
    vx_h
  );
  assert_that_double(p_star, is_equal_to_double(4 * P / 3));

  p_star = p_star_init_guess(
    &(hydro_t) {.rho = RHO, .u = VX, .p = P},
    &(hydro_t) {.rho = RHO, .u = VX, .p = 5 * P},
    vx_h
  );
  assert_that_double(p_star, is_equal_to_double(5 * P / 3));
}
