#ifndef _EINFELDTH_TEST_H_
#define _EINFELDTH_TEST_H_

/*
 * Einfeldt, B., et al. 1991
 * The solution of this problem consists of two strong rarefactions and a
 * trivial stationary contact discontinuity. The intermediate state 
 * pressure p_∗ is very small, close to vacuum, and this can lead to 
 * difficulties in the iteration scheme to find p_∗ numerically.
 */


#include <HydroBase.h>
#include <RiemannProblem.h>


hydro_t einfeldt_left = {
  .rho = 1.0,
  .u = -2.0,
  .v = 0.0,
  .w = 0.0,
  .p = 0.4
};

hydro_t einfeldt_right = {
  .rho = 1.0,
  .u = 2.0,
  .v = 0.0,
  .w = 0.0,
  .p = 0.4
};

rp_star_region_t einfeldt_star = {
  .p = 0.00189,
  .u = 0.0,
  .left.is_shock = false,
  .left.rarefaction.rho = 0.02185,
  .right.is_shock = false,
  .right.rarefaction.rho = 0.02185
};


#endif /* _EINFELDTH_TEST_H_ */
