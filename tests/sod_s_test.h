#ifndef _SODS_TESTS_H_
#define _SODS_TESTS_H_

/*
 * Sod’s shock tube problem
 * This problem can be described using the Euler equations for its time 
 * evolution. This leads to three characteristics  describing  the 
 * propagation  speed  of  the  different  regions  of  the  system.
 * These are  the  rarefaction  wave,  the  contact  discontinuity  and  the 
 * shock  discontinuity
 * Solving  this numerically it gives information on how well a scheme 
 * captures and resolves shocks and contact discontinuities and how well
 * the correct density of the rarefaction wave is reproduced. This will be 
 * used as the main test for the schemes.
 * */


#include <HydroBase.h>
#include <RiemannProblem.h>


hydro_t sod_left = {
  .rho = 1.0,
  .u = 0.0,
  .v = 0.0,
  .w = 0.0,
  .p = 1.0
};

hydro_t sod_right = {
  .rho = 0.125,
  .u = 0.0,
  .v = 0.0,
  .w = 0.0,
  .p = 0.1
};

rp_star_region_t sod_star = {
  .p = 0.30313,
  .u = 0.92745,
  .left.is_shock = false,
  .left.rarefaction.rho = 0.42632,
  .right.is_shock = true,
  .right.shock.rho = 0.26557
};


#endif /* _SODS_TESTS_H_ */
