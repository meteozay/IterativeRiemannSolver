#ifndef _TWO_SHOCKS_TEST_H_
#define _TWO_SHOCKS_TEST_H_

/*
 * Right and left shocks emerging from the solution to the left and right 
 * sections of the blast wave problem
 */


#include <stdbool.h>
#include <HydroBase.h>
#include <RiemannProblem.h>


hydro_t two_shocks_left = {
  .rho = 5.99924,
  .u = 19.5975,
  .v = 0.0,
  .w = 0.0,
  .p = 460.894
};

hydro_t two_shocks_right = {
  .rho = 5.99924,
  .u = -6.19633,
  .v = 0.0,
  .w = 0.0,
  .p = 46.0950
};

rp_star_region_t two_shocks_star = {
  .p = 1691.64,
  .u = 8.68975,
  .left.is_shock = true,
  .left.shock.rho = 14.2823,
  .right.is_shock = true,
  .right.shock.rho = 31.0426
};


#endif /* _TWO_SHOCKS_TEST_H_ */
