# Iterative Riemann Solver
Iterative Riemann solver (based on Newton-Raphson iteration method)

## Riemann problem (from Toro)
  The Riemann problem for the hyperbolic, constant coefficient system is a
  especial Initial Value Problem (IVP) describes as,

  ```math
  \begin{aligned}
  & \partial_t U + \mathbf{A} \partial_x U = 0 \\
  & U(x,0) = U_0(x) = \begin{cases}
  U_L & \quad \text{if} \quad x < 0 \\
  U_R & \quad \text{if} \quad x > 0
  \end{cases}
  \end{aligned}
  ```

  We assume that the system is strictly hyperbolic, meaning the eigenvalues
  of the Jacobian matrix $`\mathbf{A} = \partial_{u_j} f_i`$ are real and
  distinct as,

  ```math
  \lambda_1 < \lambda_2 < ... < \lambda_m
  ```

  The structure of the solution of the Riemann problem in the $`x-t`$ plane is
  depicted below,

  ![x-t-plane](assets/x-t-plane.png)

  It consists of waves emanating from the origin one for each eigenvalue
  $`\lambda_i`$ (wave propagation speeds).

## Description of the Riemann solver (from Toro and Falcinelli et al. 2012)
  The Riemann problem for the one-dimensional time-dependent Euler equations
  is the IVP for the conservation laws,

  ```math
  \partial_t U + \partial_x F(U) = 0
  ```

  $`U`$ is the vector of conservative variables and $`F(U)`$ is the vector of
  the fluxes:

  ```math
  U = \begin{bmatrix}
  \rho \\
  \rho u \\
  E
  \end{bmatrix}, F(U) = \begin{bmatrix}
  \rho u \\
  \rho u^2 + p \\
  (E + p) u
  \end{bmatrix}

  ```

  where $`E = \frac 1 2 \rho u^2 + e_{int}`$ and $`e_{int}`$ is the internal
  energy.

  The solution of this Riemann problem directly depends on the $`x/t`$ ratio
  and consists of three types of waves; two nonlinear, shocks or rarefactions,
  and one linearly degenerate, the contact discontinuity.
  
  Contact disontinuities are surfaces that separate zones of different density
  and temperature. By definition such a surface is in pressure equilibrium, and
  no gas flows across it. Usually, when the tangential component of velocity
  on one side differs considerably from that of the gas on the other side, the
  surface is called a slip discontinuity. The boundary of a supersonic jet and
  the ambient gas is an example of a slip discontinuity.

  The other two types of nonlinear waves arise from abrupt changes in pressure.
  Shock fronts acompany compression of the medium and rarefactions acompany
  expansion of the medium.
  
  These waves are
  separating four constant states where the conservative vector $`U`$ aquires
  from the left ot the right the following values, $`U_L`$, $`U_{L^*}`$,
  $`U_{R^*}`$ and $`U_R`$. The symbole $`*`$ identify points located in the
  star region between the nonlinear waves.

  Two equations listed below help us to solve the Rieman problem,

  ```math
  f_L(p_*, U_L) + f_R(p_*, U_R) + \Delta u = 0
  ```

  ```math
  u^* = \frac 1 2 (u_L + u_R) + \frac 1 2 \left[ f_R(p_*, U_R) - f_L(p_*, U_L) \right]
  ```

  The functions $`f_L`$ and $`f_R`$ represent relations across the left and the
  right nonlinear waves and are given by:

  ```math
  f_K \left(p_*, U_K\right) = \begin{cases}
    (p_* - p_K) \sqrt{(\frac{A_K}{p_* + B_K})} & \text{if } p_* \geq p_K \\
      \\
    \frac{2 c_s^K}{\gamma - 1} \left[
      \left( \frac{p_*}{p_K} \right)^{\frac{\gamma - 1}{2 \gamma}}
      - 1 \right] & \text{if } p_* < p_K
  \end{cases}
  ```

  where $`A_K = \frac{2}{(\gamma + 1) \rho_K}`$ and
  $`B_K = \frac{\gamma - 1}{\gamma + 1} p_K`$. $`p_*`$ can be solved by an
  iteration scheme using the first solution equation. Note that $`p_*`$ is 
  a function of five variables, $`p_* (\Delta u, \rho_L, p_L, \rho_R, p_R)`$.
  After finding $`p_*`$, $`u*`$ can easly be calculated by the second
  solution equation. The remaining unknowns are found by the means of standard
  gas dynamic relations.

  Following figure depicts the star region, shock (S) or rarefaction (R)
  nonlinear waves, contact discontinuity (C) and flux functions in the $`x-t`$
  plane,

  ![x-t-Riemann-Solver](assets/x-t-Riemann-Solver.jpg)

  The unknown region between $`u_L`$ and $`u_R`$ is the star region. Note that
  the middle wave is always a contact discontinuity while the left and right
  nonlinear waves are either shock or rarefaction waves. Therefore, according
  to the type of the nonlinear waves, there can be four possible wave patterns,

  ![x-t-Riemann-Solver](assets/four-cases.png)

  Note that both pressure $`p_*`$ and velocity $`u_*`$ between left and right
  waves are constant, while the density takes on the two constant values
  $`\rho_{*L}`$ and $`\rho_{*R}`$.

  So far, using our algorithm, we found $`p_*`$ and $`u_*`$. By identifying
  the type of the left or right nonlinear wave, we are able now to compute
  $`\rho_{*L}`$ and $`\rho_{*R}`$. In order to specify the type of nonlinear
  waves, we should compare the pressure $`p_*`$ with the pressures $`p_L`$ and
  $`p_R`$. For shock waves we only need to find the density behind the wave
  and the shock speed. For rarefaction waves there is more work involved; we
  need the density behind the wave, equation for the head and tail of the
  wave and the full solution inside the rarefaction fan.

  **Left shock wave**

  In the case of a left shock wave, the pressure $`p_* > p_L`$. In this case,
  the density $`\rho_{*Lj}`$ can be compute as,

  ```math
  \rho_{*L} = \rho_L \left[
    \frac{\frac{p_*}{p_L} + \frac{\gamma - 1}{\gamma + 1}}
      {\frac{\gamma - 1}{\gamma + 1}\frac{p_*}{p_L} + 1}
  \right]
  ```

  and also the shock speed $`S_L`$ is,

  ```math
  S_L = u_L - Q_L / \rho_L, \quad Q_L = \left[ \frac{p_* + B_L}{A_L} \right]^{\frac 1 2}
  ```

  Therefore, $`S_L`$ can be written as,

  ```math
  S_L = u_L - c_s^L \left[
    \frac{\gamma + 1}{2\gamma} \frac{p_*}{p_L} + \frac{\gamma - 1}{2\gamma}
    \right]^{\frac 1 2}
  ```

  **Left Rarefaction wave**

  The left rarefaction wave can be identified by the condition
  $`p_* \leq p_L)`$. The density follows from the isentropic law as,

  ```math
  \rho_{*L} = \rho_L \left( \frac{p_*}{p_L} \right)^{\frac{1}{\gamma}}
  ```
  The sound speed behind the rarefaction is,

  ```math
  c_s^{*L} = c_s^L \left( \frac{p_*}{p_L} \right)^{\frac{\gamma - 1}{2\gamma}}

  ```

  In this case, the rarefaction wave is enclosed by the /Head/ and /Tail/,
  which are the characteristics of speed given respectively by,

  ```math
  S_{HL} = u_L - c_s^L, S_{TL} = u_* - c_s^{*L}
  ```

  **Right shock wave**

  A right shock wave happens when $`p_* > p_R`$. In this case, the density
  $`\rho_{*R}`$ can be written as,

  ```math
  \rho_{*R} = \rho_R \left[
    \frac{\frac{p_*}{p_R} + \frac{\gamma - 1}{\gamma + 1}}
      {\frac{\gamma - 1}{\gamma + 1}\frac{p_*}{p_R} + 1}
  \right]
  ```

  and the shock speed is,

  ```math
  S_R = u_R + Q_R / \rho_R, \quad Q_R = \left[ \frac{p_* + B_R}{A_R} \right]^{\frac 1 2}
  ```

  So,

  ```math
  S_R = u_R - c_s^R \left[
    \frac{\gamma + 1}{2\gamma} \frac{p_*}{p_R} + \frac{\gamma - 1}{2\gamma}
    \right]^{\frac 1 2}
  ```

  **Right Rarefaction wave**

  A right rarefaction wave is identified by $`p_* \leq p_R)`$. In this case,
  the density is found from the isentropic law as,

  ```math
  \rho_{*R} = \rho_R \left( \frac{p_*}{p_L} \right)^{\frac{1}{\gamma}}
  ```
  The sound speed behind the rarefaction is,

  ```math
  c_s^{*R} = c_s^R \left( \frac{p_*}{p_L} \right)^{\frac{\gamma - 1}{2\gamma}}

  ```

  The rarefaction wave is enclosed by the /Head/ and /Tail/,
  which are the characteristics of speed given respectively by,

  ```math
  S_{HR} = u_R - c_s^R, S_{TR} = u_* - c_s^{*R}
  ```

## Testing

  Using the right state (or the state with the higher pressure in case we
  want to exploit the symmetrical cases) as the reference, we can determine
  the solution of a particular Riemans problem based on the following
  dimensionless parameters,

  ```math
  \pi_1 = \frac{\Delta u}{\sqrt{\frac{p_R}{\rho_R}}}, \pi_2 = \frac{p_L}{p_R},
  \pi_3 = \frac{\rho_L}{\rho_R}
  ```

  Diferent Riemann problems with identical values of $`\pi_1`$, $`\pi_2`$ and
  $`\pi_3`$ must have similar behavior and the calculated relation $`p_*/p_R`$
  must have the same value for all cases.

## Finding $`p_*`$ iteratively
  Our aim is to minimize or cnacel the residual, $`R(p)`$, of the following
  formula using an iterative scheme (usually Newton-Raphson iteration method):

  ```math
  f_L(p, U_L) + f_R(p, U_R) + \Delta u = R(p)
  ```
  
  Newton-Raphson method requires the calculations of the functions as well as
  of its derivative. The derivatives are calculated as:

  ```math
  f_K^\prime \left(p_*, U_K\right) =
  \begin{cases}
    \sqrt{(\frac{A_K}{p + B_K})} \left( 1 - \frac{p - p_K}{2 (B_k + p)} \right)
      & \text{if } p \geq p_K \\
    &  \\
    \frac{1}{\rho_K c_s^K} 
      \left( \frac{p}{p_K} \right)^{\frac{- (\gamma + 1)}{2 \gamma}}
      & \text{if } p < p_K
  \end{cases}
  ```

  The most computatioanly expensive steps are the calculation of the powers
  with fractional exponents, however we can avoid repeating these calculations
  by taking the similarity of the derivatives in comparison with the functions
  into the account.

  The Newton-Raphson iteration can be written as,

  ```math
  p_{i+1} = p_i - \frac{R_{(p_i)}}{R^\prime_{(p_i)}}
  ```

## Finding $`u_*`$ based on a given $`p_*`$
  When we find $`p_*`$, we are ready to calculate the velocity in the star
  region, $`u_*`$, as follow,

  ```math
  u_* = \frac 1 2 \left( u_R - u_L \right) + \frac 1 2 \left(f_R - f_L \right)
  ```
