/*
 * star_region_iterative.c
 * tests_file: star_region_iterative_tests.c
 *
 * Calculate p_star and other star region variables based on Newton-Raphson
 * iteration method
 *
 * @param: ql: Hydro state at the left side of the interface
 * @param: qr: Hydro state at the right side of the interface
 * @param: v_idx: Velocity index (specifies the direction of interest)
 * @param: niter: Number of iteration
 * @param: qstar: Star region hydro state
 *
 * @return: void (side effect: filling qstar)
 *
 * TODO: Calculate rho_star using gas dynamic relations.
 *
 * Note: Negative values of qstar->p is replaced by p_min
 * Note: Off direction velocities are set to 0.0
 */


#include <math.h>
#include <stdbool.h>
#include <IdealGas.h>
#include "./star_region_iterative.h"


#define max(x, y) (((x) > (y)) ? (x) : (y))
#define A_K(rho) (2.0 / (g_p_1 * rho))
#define B_K(p) (g_m_1__g_p_1 * p)
#define c_s(rho, p) (sqrt(gamma * p / rho))


typedef struct nonlinear_waves
{
  double v, prime;
} nonlinear_waves;


static void nonlinear_waves_calc(double, double, double,
    struct nonlinear_waves*, double[2]);


void star_region_iterative(hydro_t *left, hydro_t *right, int v_idx,
    double p_star_0, int niter, rp_star_region_t *star)
{
  double p_star = p_star_0;
  double cl[2], cr[2];
  struct nonlinear_waves fL, fR;

  for (int i = 0; i < niter; i++)
  {
    nonlinear_waves_calc(left->rho, left->p, p_star, &fL, cl);
    nonlinear_waves_calc(right->rho, right->p, p_star, &fR, cr);

    p_star -= (fL.v + fR.v + (right->vars[v_idx] - left->vars[v_idx]))
      / (fL.prime + fR.prime);
  }

  star->p = (p_star > p_min) ? p_star : p_min;

  star->u = 0.5 * (right->vars[v_idx] + left->vars[v_idx]) + 0.5 * (fR.v - fL.v);

  double p_star__p_l = p_star / left->p;
  double p_star__p_r = p_star / right->p;

  /* Four cases */
  if (p_star > left->p)
  {
    star->left.is_shock = true;

    star->left.shock.rho = left->rho
      * (p_star__p_l + g_m_1__g_p_1) / (g_m_1__g_p_1 * p_star__p_l + 1.0);

    star->left.shock.speed = left->vars[v_idx]
      - sound_speed(left) * sqrt(g_p_1__2_g * p_star__p_l + g_m_1__2_g);
  }
  else
  {
    star->left.is_shock = false;

    star->left.rarefaction.rho = left->rho * pow(p_star__p_l, gamma_inv);
    star->left.rarefaction.cs = sound_speed(left) * pow(p_star__p_l, g_m_1__2_g);
    star->left.rarefaction.speed_H = left->vars[v_idx] - sound_speed(left);
    star->left.rarefaction.speed_T = star->u - star->left.rarefaction.cs;
  }

  if (p_star > right->p)
  {
    star->right.is_shock = true;

    star->right.shock.rho = right->rho
      * (p_star__p_r + g_m_1__g_p_1) / (g_m_1__g_p_1 * p_star__p_r + 1.0);

    star->right.shock.speed = right->vars[v_idx]
      + sound_speed(right) * sqrt(g_p_1__2_g * p_star__p_r + g_m_1__2_g);
  }
  else
  {
    star->right.is_shock = false;

    star->right.rarefaction.rho = right->rho * pow(p_star__p_r, gamma_inv);
    star->right.rarefaction.cs = sound_speed(right) * pow(p_star__p_r, g_m_1__2_g);
    star->right.rarefaction.speed_H = right->vars[v_idx] + sound_speed(right);
    star->right.rarefaction.speed_T = star->u + star->right.rarefaction.cs;
  }
}


static void nonlinear_waves_calc(double rho, double p, double p_star,
    struct nonlinear_waves *f, double c[2])
{
  if (p_star > p)
  {
    f->v = (p_star - p) * sqrt(A_K(rho) / (B_K(p) + p_star));
    f->prime = sqrt(A_K(rho) / (B_K(p) + p_star)) * (1.0 - (p_star - p) / (2.0 * (B_K(p) + p_star)));
  }
  else
  {
    /*
     * c1 = (p_* / p)^(-1 / (2 * gamma))
     * c2 = (p_* / p)^(gamma / (2 * gamma)) = sqrt(p_* / p
     *
     * => (p_* / p)^((gamma - 1) / (2 * gamma)) = c1 * c2
     * => (p_* / p)^(- (gamma + 1) / (2 * gamma)) = c1 / c2
     */

    c[0] = pow(p_star / p, -1.0 / (2.0 * gamma));
    c[1] = sqrt(p_star / p);

    f->v = 2.0 * c_s(rho, p) / g_m_1 * ((c[0] * c[1]) - 1.0);
    f->prime = (p_star == 0) ? 0.0 : 1.0 / (rho * c_s(rho, p)) * c[0] / c[1];
  }
}

