/*
 * p_star_init_guess.c
 * tests_file: p_star_init_guess_tests.c
 *
 * Initial guess for star region p (p_star)
 *
 * @param: ql: Hydro state at the left side of the interface
 * @param: qr: Hydro state at the right side of the interface
 * @param: v_idx: Velocity index (specifies the direction of interest)
 *
 * @return: Initial guess of p_star
 */


#include <IdealGas.h>
#include "./p_star_init_guess.h"

double p_star_init_guess(hydro_t *ql, hydro_t *qr, int v_idx)
{
  double rho_cs[2];
  rho_cs[0] = ql->rho * sound_speed(ql);
  rho_cs[1] = qr->rho * sound_speed(qr);

  return (
    rho_cs[1] * ql->p
    + rho_cs[0] * qr->p
    + rho_cs[0] * rho_cs[1] * (ql->vars[v_idx] -  qr->vars[v_idx])
  ) / (rho_cs[0] + rho_cs[1]);
}
