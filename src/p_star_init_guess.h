#ifndef _P_STAR_INIT_GUESS_H_
#define _P_STAR_INIT_GUESS_H_


#include <HydroBase.h>


#ifdef __cplusplus
extern "C" {
#endif

double p_star_init_guess(hydro_t *ql, hydro_t *qr, int v_idx);

#ifdef __cplusplus
}
#endif


#endif /* _P_STAR_INIT_GUESS_H_ */
